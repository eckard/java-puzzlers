package net.eckard.java.puzzlers;

import org.junit.jupiter.api.Test;

import java.text.Normalizer;

import static org.assertj.core.api.Assertions.assertThat;

final class Diacritics {
    @Test
    final void acute() {
        final String spanishCity = "Cádiz";
        assertThat(spanishCity.length()).isEqualTo(5);
        System.out.println(detailString(spanishCity));

        final String spanishCity2 = "Cádiz";
        assertThat(spanishCity2).isNotEqualTo(spanishCity);
        assertThat(spanishCity2.length()).isEqualTo(6);
        System.out.println(detailString(spanishCity2));

        assertThat(Normalizer.isNormalized(spanishCity, Normalizer.Form.NFC)).isTrue();
        assertThat(Normalizer.isNormalized(spanishCity2, Normalizer.Form.NFC)).isFalse();

        assertThat(Normalizer.normalize(spanishCity2, Normalizer.Form.NFC)).isEqualTo(spanishCity);
    }

    @Test
    void trema() {
        final String germanCity = "Köln";
        assertThat(germanCity.length()).isEqualTo(4);
        System.out.println(detailString(germanCity));

        final String germanCity2 = "Köln";
        assertThat(germanCity2).isNotEqualTo(germanCity);
        assertThat(germanCity2.length()).isEqualTo(5);
        System.out.println(detailString(germanCity2));

        final String germanCity3 = "K͏̈oln";
        assertThat(germanCity3).isNotEqualTo(germanCity);
        assertThat(germanCity3).isNotEqualTo(germanCity2);
        assertThat(germanCity3.length()).isEqualTo(6);
        System.out.println(detailString(germanCity3));

        assertThat(Normalizer.isNormalized(germanCity, Normalizer.Form.NFC)).isTrue();
        assertThat(Normalizer.isNormalized(germanCity2, Normalizer.Form.NFC)).isFalse();
        assertThat(Normalizer.isNormalized(germanCity3, Normalizer.Form.NFC)).isTrue();

        assertThat(Normalizer.normalize(germanCity2, Normalizer.Form.NFC)).isEqualTo(germanCity);
        assertThat(Normalizer.normalize(germanCity3, Normalizer.Form.NFC)).isNotEqualTo(germanCity);
    }

    private static String detailString(String value) {
        final StringBuffer result = new StringBuffer(value)
                .append(" - length: ")
                .append(String.format("%01d", value.length()))
                .append(" - unicode: ");
        for (char c : value.toCharArray()) {
            result.append(String.format("\\u%04x ", (int)c));
        }
        return result.toString();
    }
}